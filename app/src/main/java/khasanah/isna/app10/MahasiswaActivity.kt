package khasanah.isna.app10

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_mahasiswa.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class MahasiswaActivity: AppCompatActivity(), View.OnClickListener {

    lateinit var mediaHelper: MediaHelper
    lateinit var mhsAdapter : AdapterDataMhs
    lateinit var prodiAdapter : ArrayAdapter<String>
    lateinit var kelaminAdapter : ArrayAdapter<String>
    var daftarMhs = mutableListOf<HashMap<String,String>>()
    var daftarProdi = mutableListOf<String>()
    var daftarKelamin = mutableListOf<String>()
    var url = "http://192.168.43.13/kampus/show_data.php"
    var url2 = "http://192.168.43.13/kampus/get_nama_prodi.php"
    var url3 = "http://192.168.43.13/kampus/query_upd_del_ins.php"
    var url4 = "http://192.168.43.13/kampus/get_jenis_kelamin.php"
    var imStr = ""
    var pilihProdi = ""
    var pilihKelamin = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mahasiswa)
        mhsAdapter = AdapterDataMhs(daftarMhs,this) //new
        mediaHelper = MediaHelper(this)
        listMhs.layoutManager = LinearLayoutManager(this)
        listMhs.adapter = mhsAdapter

        prodiAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarProdi)
        spinProdi.adapter = prodiAdapter
        spinProdi.onItemSelectedListener = itemSelected

        kelaminAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarKelamin)
        spinKelamin.adapter = kelaminAdapter
        spinKelamin.onItemSelectedListener = itemSelected1


        imUpload.setOnClickListener(this)
//        listMhs.addOnItemTouchListener(itemTouch) #new
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDataMhs("")
        getNamaProdi()
        getJenisKelamin()
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinProdi.setSelection(0)
            pilihProdi = daftarProdi.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihProdi = daftarProdi.get(position)
        }

    }
    val itemSelected1 = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {

            spinKelamin.setSelection(0)
            pilihKelamin = daftarKelamin.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihKelamin = daftarKelamin.get(position)
        }

    }

//    val itemTouch = object : RecyclerView.OnItemTouchListener{
//        override fun onTouchEvent(p0: RecyclerView, p1: MotionEvent) {}
//        override fun onInterceptTouchEvent(p0: RecyclerView, p1: MotionEvent): Boolean {
//            val view = p0.findChildViewUnder(p1.x,p1.y)
//            val tag = p0.getChildAdapterPosition(view!!)
//
//            val pos = daftarProdi.indexOf(daftarMhs.get(tag).get("nama_prodi"))
//            spinProdi.setSelection(pos)
//            edNim.setText(daftarMhs.get(tag).get("nim").toString())
//            edNamaMhs.setText(daftarMhs.get(tag).get("nama").toString())
//            Picasso.get().load(daftarMhs.get(tag).get("url")).into(imUpload)
//
//            return false
//        }
//
//        override fun onRequestDisallowInterceptTouchEvent(p0: Boolean) {}
//
//    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if (requestCode == mediaHelper.getRcGallery()){
                imStr = mediaHelper.getBitmapToString(data!!.data!!,imUpload)
            }
        }
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,url3,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi berhasil", Toast.LENGTH_LONG).show()
                    showDataMhs("")
                }else{
                    Toast.makeText(this,"Operasi GAGAL", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date())+".jpg"
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("nim",edNim.text.toString())
                        hm.put("nama",edNamaMhs.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_prodi",pilihProdi)
                        hm.put("kelamin",pilihKelamin)
                        hm.put("alamat",edAlamat.text.toString())
                    }
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("nim",edNim.text.toString())
                        hm.put("nama",edNamaMhs.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_prodi",pilihProdi)
                        hm.put("kelamin",pilihKelamin)
                        hm.put("alamat",edAlamat.text.toString())
                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("nim",edNim.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getNamaProdi(){
        val request = StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarProdi.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarProdi.add(jsonObject.getString("nama_prodi"))
                }
                prodiAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getJenisKelamin(){
        val request = StringRequest(
            Request.Method.POST,url4,
            Response.Listener { response ->
                daftarKelamin.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarKelamin.add(jsonObject.getString("kelamin"))
                }
                kelaminAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataMhs(namaMhs: String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarMhs.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mhs = HashMap<String,String>()
                    mhs.put("nim",jsonObject.getString("nim"))
                    mhs.put("nama",jsonObject.getString("nama"))
                    mhs.put("nama_prodi",jsonObject.getString("nama_prodi"))
                    mhs.put("url",jsonObject.getString("url"))
                    mhs.put("alamat",jsonObject.getString("alamat"))
                    mhs.put("kelamin",jsonObject.getString("kelamin"))
                    daftarMhs.add(mhs)
                }
                mhsAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("nama",namaMhs)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imUpload ->{
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,mediaHelper.getRcGallery())
            }
            R.id.btnInsert ->{
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUpdate ->{
                queryInsertUpdateDelete("update")
            }
            R.id.btnDelete ->{
                queryInsertUpdateDelete("delete")
            }
            R.id.btnFind ->{
                showDataMhs(edNamaMhs.text.toString().trim())
            }
        }
    }
}