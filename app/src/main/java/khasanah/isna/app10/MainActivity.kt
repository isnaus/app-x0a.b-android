package khasanah.isna.app10

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnMhs.setOnClickListener(this)
        btnProdi.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnMhs ->{
                var intent = Intent(this,MahasiswaActivity::class.java)
                startActivity(intent)
            }
            R.id.btnProdi ->{
                var intent = Intent(this, ProdiActivity::class.java)
                startActivity(intent)
            }
        }
    }
}
